@extends('dashboard')
@section('content')
<form method="POST" action="{{ route('store') }}" enctype="multipart/form-data" >
@csrf

<div class="row">
        <div class="col-md-6">
            <lable>{{ 'Car Name' }} </lable>
            <input type="text" class="form-control" placeholder="" name="name">
        </div>
        <div class="col-md-6">
            <lable>{{ 'Car Model' }} </lable>
            <input type="text" class="form-control"  placeholder="" name="Model">
        </div>
</div>
<div class="row">
        <div class="col-md-6">
            <lable>{{ 'Car Image' }} </lable>
            <input type="file" class="form-control"  placeholder="" name="image">
        </div>
        <div class="col-md-6">
            <lable>{{ 'Car Year' }} </lable>
            <select  id="" class="form-control" name="Year">
                <option value="">-- Select Year --</option>
                    @foreach ($year as $key => $years)
                        <option value="{{$years}}"> {{$years}} </option>
                    @endforeach
            </select>
        </div>
</div>
                <div class="offset-md-5">
                    <button type="submit" style="text-align:center;margin:25px !important"><i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </button>
                </div>
    </form>

@endsection










