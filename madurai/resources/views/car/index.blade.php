  
@extends('dashboard')
@section('content')
<a style="float:right"  href="{{ route ('create')}}"><i class="fa fa-plus"  aria-hidden="true" style="font-size=30px;color:red"></i></a>
  <table id="tblCustomers" class="table">
        <thead>
            <tr>
                <th>S.NO</th>
                <th>Car Name</th>
                <th>Car Modal</th>
                <th>Car Year</th>
                <th>Activity</th>
            </tr>
        </thead>
        <tbody>
            @forelse($cars as $key => $carslist)
            <tr>
                <td> {{ $key +1 }} </td>
                <td> {{ $carslist-> name }} </td>            
                <td> {{ $carslist-> Model }} </td>            
                <td> {{ $carslist-> Year }} </td>           
                <td>
                    <a href={{ route('edit',$carslist->id) }} ><i class="fa fa-edit" aria-hidden="true"></i> </a> $$
                    <a href={{ route('delete',$carslist->id) }} onclick="return confirm('Are you sure?')" ><i class="fa fa-trash" style="color:red"aria-hidden="true"></i> </a>

                </td> 
            </tr>
            @empty

            @endforelse

        </tbody>
    </table>
@endsection
<script>
    $('#tblCustomers').DataTable();
    </script>
    