<!DOCTYPE html>
<html>
<head>
    <title>sam Project Crud</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" >
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <link  href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <nav class="navbar navbar-light navbar-expand-lg mb-5" style="background-color: #e3f2fd;">
        <div class="container">
            <a class="navbar-brand mr-auto" href="#"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register-user') }}">Register</a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('signout') }}">Logout</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('index') }}">To See View page</a>
                    </li>
                    @endguest
                </ul>
                
            </div>
        </div>
    </nav>
    @yield('content')
    <!-- <div class="row">
                <div class="col-md-3">
                    <label>Model</label>
                    <select class="form-control  btn-secondary" name="model">
                        <option >--Select--</option>
                        <option value="1">Maruti Suzuki Ciaz</option>
                        <option value="2">Datsun GO+</option>
                        <option value="3">Datsun GO</option>
                    </select>
                </div>
                <div class="col-md-3">
                <label>Year</label>
                    <select class="form-control btn-secondary" name="year">
                        <option >--Select--</option>
                        <option value="1">2019</option>
                        <option value="2">2020</option>
                        <option value="3">2021</option>
                    </select>
                </div>
                <div class="col-md-3">
                <label>color</label>
                    <select class="form-control btn-secondary" name="color">
                        <option >--Select--</option>
                        <option value="1">Red</option>
                        <option value="2">Green</option>
                        <option value="3">Bule</option>
                    </select>
                </div>
                <div class="col-md-3">
                <label>Mileage</label>
                    <select class="form-control  btn-secondary" name="mile">
                        <option >--Select--</option>
                        <option value="1">60km</option>
                        <option value="2">70km</option>
                        <option value="3">70km</option>
                    </select>
                </div>
            </div> -->
</body>
</html>