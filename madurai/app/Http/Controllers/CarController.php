<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;
use carbon\Carbon;

class CarController extends Controller
{
    public function index()
    {
        // $current_year = Carbon::now()->year;
        // $current_month = Carbon::now()->month;

        // $date = Carbon::create($current_year, $current_month)->startOfMonth();

        // $sameMonthLastYear = $date->copy()->subYear()->format('Ym');
        // $lastMonthYear =  $date->copy()->subMonth()->format('Ym');
        // $LastToLastMonthYear = $date->copy()->subMonths(2)->format('Ym');

        // dd($sameMonthLastYear);
        
        $data = [
            'cars' =>Car::get(),
        ];

        return view('car.index',$data);
    }

    public function create(Request $request)
    {
        $data = [
            'year' =>array('2022','2021'),
        ];

        return view('car.create',$data);
    }

    public function store(Request $request)
    {

        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
        
        Car::create([
            'name' => $request->name,
            'Model' => $request->Model,
            'Year' => $request->Year,
            'image' => $imageName,
        ]);

        return redirect(route('index'));
    }

    public function edit(Request $request,$id)
    {
        $data = [
            'car' => Car::where('id',$id)->first(),
            'year' =>array('2022','2021'),

        ];
        return view('car.edit',$data);
      
    }

    public function update(Request $request,$id)
    {

       Car::where('id',$id)->update([
        'name' => $request->name,
        'Model' => $request->Model,
        'Year' => $request->Year,
    ]);

    return redirect(route('index'));

    }

    public function delete($id)
    {
        Car::where('id',$id)->delete();

        return redirect(route('index'));

    }
}
