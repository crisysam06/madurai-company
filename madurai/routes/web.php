<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CarAuthController;
use App\Http\Controllers\CarController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('dashboard', [CarAuthController::class, 'dashboard']); 
Route::get('login', [CarAuthController::class, 'index'])->name('login');
Route::post('custom-login', [CarAuthController::class, 'customLogin'])->name('login.custom'); 
Route::get('registration', [CarAuthController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [CarAuthController::class, 'customRegistration'])->name('register.custom'); 
Route::get('signout', [CarAuthController::class, 'signOut'])->name('signout');

// Car Controller
Route::get('index', [CarController::class, 'index'])->name('index');
Route::get('create', [CarController::class, 'create'])->name('create');
Route::post('store', [CarController::class, 'store'])->name('store');
Route::get('edit/{id}', [CarController::class, 'edit'])->name('edit');
Route::post('update/{id}', [CarController::class, 'update'])->name('update');
Route::get('delete/{id}', [CarController::class, 'delete'])->name('delete');



